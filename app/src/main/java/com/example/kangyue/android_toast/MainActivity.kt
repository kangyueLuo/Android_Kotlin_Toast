package com.example.kangyue.android_toast

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var btnToastLong : Button ? = null
    private var btnToastShort :Button ? = null
    private var tvMsg :TextView ? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnToastLong = findViewById(R.id.btn_long_toast)
        btnToastShort = findViewById(R.id.btn_short_toast)
        tvMsg = findViewById(R.id.msg)

        btnToastLong!!.setOnClickListener(){
            showLongToast()
        }

        btnToastShort!!.setOnClickListener(){
            showShortToast()
        }

    }

    private fun showLongToast(){
        tvMsg!!.setText(R.string.msg_long_toast);
        Toast.makeText(applicationContext,R.string.msg_long_toast,Toast.LENGTH_LONG).show()
    }

    private fun showShortToast(){
        tvMsg!!.setText(R.string.msg_short_toast);
        Toast.makeText(applicationContext,R.string.msg_short_toast,Toast.LENGTH_SHORT).show()
    }
}
